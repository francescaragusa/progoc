## Progetto di Ottimizzazione Combinatoria: implementazione dell'algoritmo Blossom di Edmonds

####Definizione del problema
<div style="text-align: justify">

Il problema dell'accoppiamento (o $matching$) è un problema di ottimizzazione su grafi in cui si cerca di trovare il maggior numero di archi a due a due non incidenti.
Formalmente, dato un grafo $G= (V,E)$, il problema è definito da:


>$ MAX \sum_{e \in E} y_e$
>
>$\sum_{e \in \delta(v)} y_e \leq 1, \forall v \in V$
>
>$ 0 \leq y_e \leq 1$

####Cammini aumentanti

Fissato un matchig $M$ su un grafo $G$, un arco $y$ è _accoppiato_ se $y \in M$. Di conseguenza, un nodo è _accoppiato_ se $\exist y \in \delta(v)$ tale che $y \in M$. Altrimenti, il nodo è _esposto_.

Un cammino è _alternato_ quando alterna archi accoppiati ad archi non accoppiati.
Un cammino è _aumentante_ quando è alternante e gli estremi sono due vertici esposti. E' chiaro che ogni cammino aumentante deve avere lunghezza dispari.

> **Teorema 1**: Sia $M$ un matching di $G$. Se esiste un cammino aumentante all'interno del grafo, allora esiste anche un matching $M_i = (M \setminus P) \cup (P \setminus M)$ avente cardinalità $|M+1|$.

Da cui segue:

> **Teorema 2**: Sia $M$ un matching di $G$. Se non esistono cammini aumentanti, allora il matching è massimo.


####Algoritmo di Edmonds

Tenendo a mente il _Teorema 2_, è quindi possibile ottenere il massimo matching continuando ad aumentare il matching corrente lungo i cammini aumentanti. Nel momento in cui non saranno più presenti nel grafo cammini aumentanti, avremo trovato il massimo matching. Questa procedura può essere formalizzata come segue:

```py
ALGORITMO PER IL MASSIMO MATCHING:
    M = {}
    while(esiste un cammino aumentante P)
        aumenta il matching lungo P
```

Rimane da definire come trovare un cammino aumentate e come dimostrare che non ne esiste nemmeno uno. Nel caso di grafi bipartiti è sufficiente segnare i vertici raggiungibili da un vertice non coperto dal matchig con una progressione alternante di archi, e dal momento che non sono presenti circuiti dispari, i vertici raggiungibili da una progressione alternante erano anche raggiungibili da un cammino alternante. Tuttavia, questo non è più valido nel caso di un grafo generico.

##### Ricerca di cammini alternanti

Per scovare cammini alternanti all'interno del grafo, si costruisce una foresta _alternante_ rispetto al matching corrente.

Una foresta $F$ è _alternante_ rispetto a $M$ se soddisfa le seguenti proprietà:
- $V(F)$ contiene tutti i vertici esposti di $M$. Ogni componente connessa di $F$ contiene esattamente un vertice non coperto da $M$, la sua _radice_.
- Un vertice $v$ appartente a una componente connessa si dice _outer_ se è a distanza pari dalla radice, e si dice _inner_ altrimenti.
- Ogni cammino nodo-radice è $M$-alternante.


Vediamo ora come costruire la foresta:

- Prendiamo in considerazione ogni vertice esposto $S$
- $\forall s \in S$ considera un vicino $y$ di $s$. Se:
    1. $y$ non è contenuto in $V(F)$ allora aggiungiamo l'arco $s-y$ alla foresta. Si noti che in questo caso $y$ non può essere esposto, dal momento che se lo fosse allora sarebbe già contenuto all'interno della foresta. Oltre che ad $y$, aggiungiamo alla foresta anche il nodo $x$ accoppiato con $y$.
    2. $y$ è outer in una diversa componente della foresta allora abbiamo trovato un cammino aumentante da $s$ alla radice dell'albero contentente $y$.
    3. $y$ è un vertice outer ed appartiene già alla stessa componente di $s$, allora siamo in presenza di un ciclo di lunghezza dispari (_blossom_). Vedremo successivamente come gestire questo caso.

Se non si può applicare nessuno dei tre casi, allora tutti i vicini dei vertici esterni sono inner e non sono più presenti cammini aumentati. Di conseguenza, il matching corrente è massimo.

##### Blossom
Si consideri il seguente grafo (in cui gli archi del matching sono evidenziati in rosso):

```dot
graph {
    1 -- 2 ;
    2 -- 3 [color = red];
    3 -- 4 ;
    4 -- 5 [color = red];
    5 -- 3 ;
    4 -- 6 ;
    5 -- 2 ;
}
```

Il percorso _1-2-3-4-5-3-2-5-4-6_ è una progressione alternante, ma non è un cammino. Viene infatti attraversato un circuito dispari: _3-4-5_. Inoltre, nell'esempio è presente un cammino alternate, che è: _1-2-3-5-4-6_.
Il circuto dispari che abbiamo attaversato prende il nome di _blossom_. Si definisce blossom un qualsiasi circuito dispari avente un solo vertice esposto all'interno del circuito (che prende il nome di _base_).

L'algoritmo di Edmonds gesitsce i blossom come segue: quando viene individuato un blossom, lo si contrae in un unico nodo e si prosegue con la ricerca di un cammino aumentante.

> **Teorema 3** : Se esiste un cammino aumentante prima della contrazione, allora esiste anche dopo la contrazione

Infatti, contraendo il blossom di base _3_ otteniamo:

```dot
graph {
    1 -- 2 ;
    2 -- 3 [color = red];
    3 -- 6;
}
```
ed è ora pittosto facile trovare il cammino aumentante.
Nel caso in cui il nostro cammino aumentante dovesse contente un vertice contratto, sarà necessario attraversare il blossom in maniera opportuna per trovare il corretto cammino alternate.
Infatti, dal momento che il cammino totale deve avere lunghezza dispari, sarà necessario attraversare il blossom lungo un numero pari di archi. Nell'esempio, dobbiamo attraversare il blossom partendo dal nodo _2_ per terminare nel nodo _6_, quindi la sequenza corretta di nodi da attraversare è _3-5-4_ (e non _3-4_).

Se invece il cammino non contiene un nodo contratto, allora non vi è alcuna necessità di attraversare i blossom presenti.

Ogni volta che il matching viene aumentato, il grafo viene "decompresso", quindi i blossom precedentemente trovati non sono più considerati e il grafo deve essere esplorato di nuovo a partire da uno stato privo di blossom.

##### Pseudocodice

```py
funzione EDMONDS:
    M = {}
    while(esiste un cammino aumentante P) #esploraGrafo
        aumenta il matching lungo P #aumentaMatching
```
```py
funzione ESPLORA GRAFO:
    F = {} #foresta vuota
    P = {} #cammino aumentante

    for n in V:
        aggiungo n ad F
    for e in E:
        if(e in M):
            segna e come marcato

    for v in F:
        while(esiste un arco e non marcato (v,w)):
            if(w not in F): #caso 1
                aggiungo (v,w) e (w,x) ad F #aggiornaForesta
            else:
                if(la distanza tra w e radice(w) è pari):
                    if(radice(v) != radice(w)) #caso 2
                        P = c.a. da radice(v) a radice(w) #getCammino
                    else #caso 3
                        contrazione del blossom nel grado #blossom
            segna e come marcato

    return P

```

##### Complessità


Il numero massimo di vertici accoppiati in un grafo è al più $\lfloor n/2 \rfloor$ (dove $n = |V|$). Inoltre, ogni volta che aumentiamo il matching lungo un cammino aumentante, il numero di nodi nel matching aumenta di 1 (per Teorema 1). Di conseguenza si avranno al più $\lfloor n/2 \rfloor$ chiamate ad ```ESPLORA GRAFO```.

Ad ogni chiamata di ```ESPLORA GRAFO```, iteriamo su tutti i nodi della foresta, che sono al più $n$. E, per ogni nodo, iteriamo sui suoi archi non marcati del vicinato. Per mantenere la foresta, si utilizza una struttura dati di tipo _union-find_. Ora:

- se si deve aggiungere un arco alla foresta il costo è costante. Di conseguenza, se $|E| = m$, allora il costo totale sarà $\mathcal{O}(m)$.
- se si è trovato un cammino aumentante, sarà necessario ricostuirlo attraverso le chiamate ```radice(v)``` e ```radice(w)```, che restituiscono il cammino minimo tra i nodi $v$ e $w$ e le rispettive radici. Il costo di queste chiamate è al più $\mathcal{O}(n)$.
- se si è trovato un blossom, sarà necessario eseguire una contrazione del grafo: il costo sarà $\mathcal{O}(m)$.


In conclusione, la complessità totale sarà $\mathcal{O}(n \times n \times m)$.


## Implementazione

##### Generazione del grafo

E' possibile generare un grafo in due modi distinti:
- in modo pseudocasuale attraverso la procedura ```generaGrafoCasuale(int size, int densita, Grafo* grafo)```. Fissato un numero di nodi pari ad al più ```size```, il grafo viene popolato aggiungendo ogni possibile arco con una certa probabilità definita dal parametro```densita```, che deve essere compreso tra 1 e 100. Non vengono generati self loop, non vengono generati cicli contenenti solo due archi. Al termine della procedura verrà generato un file ```archi.txt``` contenente gli archi del grafo.
- attraverso una lista di archi contenuta in un file tramite la funzione ```generaGrafoDatiArchi(Grafo* grafo, string nomeFile)```.  

######Esempio
Un file contentente la seguente lista
```console
1 2
2 3
3 1
2 4
5 4
```
genererà questo grafo:


```dot
graph {
	1 -- 2;
	2 -- 3;
	3 -- 1;
	2 -- 4;
    5 -- 4;
}
```

##### Stampa del grafo

Invocando la funzione ```printaGrafo(Grafo* grafo)``` verrà generato un file ```.dot``` in cui si fornisce una rappresentazione del grafo mediante una lista di archi. Verrà inoltre visualizzato sullo schermo una rappresentazione vera e propria del grafo attraverso un'immagine ```png```.

Se ci dovessero essere problemi di compatibilità, suggerisco di commentare le seguenti linee di codice (in ```progoc/generazioneGrafo/Grafo.cpp```):

```cpp
system("dot -Tpng:cairo:gd grafo.dot -o grafo.png");
system("eog grafo.png &");
```

Il file ```.dot``` verrà generato ugualmente, ma l'immagine dovrà essere generata e aperta manualmente.

Inoltre, verrà anche generato un file denominato ```coppie.txt``` contenente le coppie appartenenti al matching (se esistenti).
Sarà possibile visualizzare tali coppie anche nell'immagine, dal momento che saranno evidenziate da degli archi rossi.


##### Rappresentazione dei blossom

Ho deciso di associare ad ogni nodo un vettore contentente i blossom a cui appartiene. Il blossom viene definito dalla base e dagli altri elementi che compongono il ciclo di lunghezza dispari. Inoltre, al fine di gestire opportunamente blossom innestati e il vinicato di questi ultimi, è necessario memorizzare anche i nodi contenuti in eventuali blossom già compressi all'interno dei nodi componenti il nuovo blossom.

Ad esempio, supponiamo di avere il seguente grafo:

```dot
graph{
    1 -- 2;
    1 -- 3;
    2 -- 3 [color = red];
    4 -- 1;
    5 -- 3;
    4 -- 5 [color = red];
    2 -- 6;
    5 -- 7;
}
```

Si può individuare un primo blossom in _1-2-3_. La base del blossom è _1_, mentre gli elementi sono _2,3_. Comprimiamo quindi il grafo:

```dot
graph{
    1 -- 6;
    1 -- 5;
    4 -- 1;
    4 -- 5 [color = red];
    7 -- 5;
}
```
Il vicinato del blossom appena trovato è {_4,5,6_}. Si noti ora che è presente un altro blossom, composto da _1,4,5_. Una volta compresso il grafo, otterremo:

```dot
graph{
    1 -- 6;
    1 -- 7;
}
```
Nonostante gli elementi di quest'ultimo blossom siano _4,5_, i nodi _effettivamente_ compressi all'interno di _1_ sono _2,3,4,5_. Di conseguenza, il vicinato di _1_ non è composto unicamente da _7_ (che è collegato a _5_), ma anche da _6_ (che è collegato a _2_).

##### Attraversamento dei blossom

Quando viene trovato un cammino aumentante, questo potrà essere composto sia da nodi semplici che da nodi compressi. Supponiamo che tali nodi siano contenuti in un vettore ```v``` di dimensione ```size```. Per attraversare correttamente il blossom, ho utilizzato la seguente procedura:

- CASO 1: il blossom è all'inizio del percorso (e quindi è in ```v[0]```). Dal monento che i cammini aumentanti hanno come estremi due nodi esposti, è necessario attraversare il blossom a partire dalla base, che per definizione è l'unico nodo esposto all'interno del blossom. A questo punto dovrò cercare all'interno del blossom quale nodo è adiacente (direttamente o indirettamente, nel caso di blossom annidiati) al nodo conentuto in ```v[1]```. Una volta trovato, si cerca all'interno del blossom un percorso di lunghezza pari che colleghi la base al nodo appena trovato.

- CASO 2: il blossom è alla fine del percorso (e quindi è in ```v[size-1]```). Come già precedentemente spiegato, il cammino aumentante dovrà terminare con un nodo esposto, quindi scegliamo la base come fine del percorso all'interno del blossom. Analogamente a prima, sarà necessario trovare all'interno del blossom il nodo adiacente a ```v[size-2]```, e poi trovare un percorso di lunghezza pari che lo colleghi con la base.

- CASO 3: il blossom si trova all'interno del cammino aumentante (quindi in ```v[i]```, con ```0 < i < size-1``` ). In questo caso sarà necessario trovare i due nodi adiacenti a ```v[i-1]``` e ```v[i+1]``` e collegarli attraverso un percorso di lunghezza pari.

Una volta trovato il percorso all'interno del blossom, sarà necessario controllare la presenza di eventuali blossom annidiati. Se ce ne dovessero essere, sarà necessario attraversarli seguendo la procedura sopra descritta, utilizzando come vettore il cammino appena trovato.

## Istruzioni

Per agevolare il processo di testing dell'algoritmo, è stato messo a disposizione un file ```main.cpp``` che svolge i seguenti compiti:
- genera il grafo, secondo le indicazioni dell'utente
- invoca la funzione ```EDMONDS``` sul grafo appena generato
- stampa il grafo contenete il matching

Per compilare correttamente il codice, occorre utilizzare il seguente comando in ```progoc/main/```:

```console
g++ ../generazioneGrafo/*.cpp ../algoritmo/*.cpp main.cpp -o main
```
A questo punto, per eseguire il programma su un grafo casuale, utilizzare il comando:
```console
./main c <#nodi> <densità>
```
Per concludere, se si vuole eseguire il programma su un grafo i cui archi sono contentuti in un file, utilizzare:
```console
./main f <nomeFile>
```
##### Esempio di esecuzione

Supponiamo di voler eseguire l'algoritmo su un grafo casuale contentente al più 10 nodi e avente $\mathcal{P}(\exist$ _arco tra_ $u$ _e_ $v) = 0.4$ $\forall u,v \in V$.
Dovremo quindi eseguire il seguente comando:
```console
./main c 10 40
```
Verrà quindi creato il file ```archi.txt``` contentente gli archi del grafo. Supponiamo che  venga generato il seguente grafo:


```dot
graph {
	1 -- 2;
	1 -- 3;
	1 -- 8;
	1 -- 10;
	2 -- 3;
	2 -- 7;
	2 -- 8;
	2 -- 10;
	3 -- 5;
	3 -- 6;
	3 -- 7;
	3 -- 9;
	3 -- 10;
	4 -- 5;
	4 -- 6;
	4 -- 7;
	4 -- 9;
	4 -- 10;
	5 -- 8;
	5 -- 9;
	5 -- 10;
	6 -- 10;
	7 -- 8;
	9 -- 10;
}
```
Sul terminale verrà stampato un report che documenta i passaggi che sono stati effettuati per ottenere il matching finale. In questo esempio si avrà il seguente report:

```console
***INIZIO***
------------
Iniziata esplorazione grafo
Trovato cammino aumentante da 1 a 2
Nodi del c.a: 1 2
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 10
	elementi: 2 1
------------
```
```console
Iniziata esplorazione grafo
Trovato cammino aumentante da 10 a 3
Attraversamento blossom
	base: 10
	elementi: 2 1
Nodi del c.a: 10 3
------------
Iniziata esplorazione grafo
Trovato cammino aumentante da 4 a 5
Nodi del c.a: 4 5
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 6
	elementi: 10 3
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 6
	elementi: 2 1
------------
Iniziata esplorazione grafo
Trovato cammino aumentante da 6 a 8
Attraversamento blossom
	base: 6
	elementi: 2 1
Attraversamento blossom
	base: 6
	elementi: 10 3
Nodi del c.a: 6 3 10 1 2 8
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 7
	elementi: 8 2
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 7
	elementi: 10 1
------------
Iniziata esplorazione grafo
Trovato blossom
	base: 7
	elementi: 6 3
------------
Iniziata esplorazione grafo
Trovato cammino aumentante da 7 a 9
Attraversamento blossom
	base: 7
	elementi: 6 3
Attraversamento blossom
	base: 7
	elementi: 10 1
Attraversamento blossom
	base: 7
	elementi: 8 2
Nodi del c.a: 7 2 8 1 10 6 3 9
------------
Iniziata esplorazione grafo
Non sono stati trovati altri cammini aumentanti
------------
Cardinalità del matching: 5
------------
***FINE***
```
A questo punto, verrà generato il file ```coppie.txt``` contentente la soluzione del matching, e il file ```grafo.dot``` contenete la rappresentazione del grafo. Verrà infine visualizzato il grafo, la cui immagine viene fornita nella pagina successiva.

#####Riferimenti bibliografici:
- Edmonds, Jack. "Paths, trees, and flowers."
- Dieter Jungnickel. "Graphs, Networks and Algorithms", chapter 13 "Matchings"
- Korte, Vygen. "Ottimizzazione Combinatoria", capitolo 10 "Massimo Matching"

```dot
graph {
	1 -- 2;
	1 -- 3;
	1 -- 8	[color=red];
	1 -- 10;
	2 -- 3;
	2 -- 7	[color=red];
	2 -- 8;
	2 -- 10;
	3 -- 5;
	3 -- 6;
	3 -- 7;
	3 -- 9	[color=red];
	3 -- 10;
	4 -- 5	[color=red];
	4 -- 6;
	4 -- 7;
	4 -- 9;
	4 -- 10;
	5 -- 8;
	5 -- 9;
	5 -- 10;
	6 -- 10	[color=red];
	7 -- 8;
	9 -- 10;
}
```
