#ifndef GRAFO_H
#define GRAFO_H

#include <vector>
#include<bits/stdc++.h>

using namespace std;

struct Nodo;
struct Arco;
struct Blossom;
struct Grafo;
struct Foresta;


struct Nodo{
    int id;
    vector<Arco*> vicinato;

    //mi identifica i blossom a cui appartiene
    //uso un vettore per gestire blossom annidiati
    bool attivo = true;
    vector<Blossom*> blossom;

    //questo lo uso per implementare la union find
    Nodo* radice = nullptr;
    Nodo* padre = nullptr;
    vector<Nodo*> figli;
    int distanza = -1;

    //mate nel matching
    Nodo* mate = nullptr;

    void clear(){
        radice = nullptr;
        padre = nullptr;
        figli.clear();
        distanza = -1;
    }
};

extern Nodo* nullo;

struct Arco{
    Nodo* u = new Nodo;
    Nodo* v = new Nodo;
    bool marcato = false;

    //dato a, da trovare b
    void estraiNodo(Nodo** a, Nodo** b){
        if(*a == u)
          *b = v;
        else
          *b = u;
    }

};

struct Blossom{
    vector<Nodo*> elementi;
    Nodo* base = new Nodo; //la tengo immutata per comodità
    set<Nodo*> innestati;
    int timestamp;

    void setInnestati(){
        for(Nodo* n : elementi){
            if((n->blossom).size() > 0 )
                for(Nodo* e : (n->blossom).back()->innestati)
                    innestati.insert(e);
            innestati.insert(n);
        }
        if((base->blossom).size() > 0 )
            for(Nodo* e : (base->blossom).back()->innestati)
                innestati.insert(e);
        innestati.insert(base);

    }

    void getVicinato(vector<Arco*> &vicinato_base, int tm){
        //cout << "tm " << tm << "\n";
        set<Nodo*> vicinato_provvisorio;
        Nodo* y = new Nodo;

        for(Nodo* x: innestati)
            for(Arco* e: x->vicinato){
                e->estraiNodo(&x,&y);
                vicinato_provvisorio.insert(y);
            }

        for(Nodo* n : vicinato_provvisorio){
            if(n->attivo){
                Arco* a = new Arco;
                a->u = base;
                a->v = n;
                vicinato_base.push_back(a);
            } else {
                y = n;
                //cout << "inizia risalita: ";
                while(y != (y->blossom).back()->base){
                    if(tm != -1 && ((y->blossom).back()->base)->blossom.back()->timestamp > tm)
                        break;
                    y = (y->blossom).back()->base;
                //    cout << " ts " << (y->blossom).back()->timestamp << "\t";

                }
                //cout << "\n";
                if(y != base){
                    Arco* a = new Arco;
                    a->u = base;
                    a->v = y;
                    if(base->mate == y)
                        a->marcato = true;
                    vicinato_base.push_back(a);
                }

            }

        }

    }

};



struct Grafo{
    vector<Nodo*> nodi;
    vector<Arco*> archi;
    int ncoppie = 0;

    void eliminaBlossom(){
        for(Nodo* n : nodi){
            n->blossom.clear();
            n->attivo = true;
        }
    }

};

struct Foresta{
    vector<Nodo*> elementi;

    //aggiungo il nodo alla foresta e lo inizializzo a dovere
    void makeset(Nodo** n){
        //cout << "creata f " << (*n)->id << "\n";
        elementi.push_back(*n);
        (*n)->radice = (*n);
        (*n)->padre = (*n);
        (*n)->distanza = 0;
    }

    Nodo* find(Nodo** n){
        return (*n)->radice;
    }

    void getFigli(Nodo** b, vector<Nodo*> &figli){
        if(! ((*b)->figli).empty()){
            for(Nodo* n : (*b)->figli)
                figli.push_back(n);
            for(Nodo* n : (*b)->figli)
                getFigli(&n, figli);
        }
    }

    //mi raccomando quando implementi attenta a cosa è a e cosa è b
    void treeUnion(Nodo** a, Nodo** b){
        //cout << "unione " << (*a)->id << " " << (*b)->id << "\n";
        (*b)->radice = (*a)->radice;
        (*b)->padre = *a;
        (*b)->distanza = ((*b)->padre)->distanza  +  1;
        vector<Nodo*> figli;
        getFigli(b, figli);
        for(Nodo* n : figli){
            n->radice = (*a)->radice;
            n->distanza = (n->padre)->distanza  +  1;
        }
        ((*a)->figli).push_back(*b);
    }


};


    void generaGrafoDatiArchi(Grafo* grafo, string nomeFile);
    void generaGrafoCasuale(int size, int densita, Grafo* grafo);
    void printGrafo(Grafo* grafo);



#endif
