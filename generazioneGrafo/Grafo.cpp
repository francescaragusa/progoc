#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include <iterator>
#include <map>
#include <algorithm>    // std::find
#include "Grafo.h"


using namespace std;


void printGrafo(Grafo* grafo){
  ofstream file;
  file.open ("grafo.dot");
  ofstream coppie;
  coppie.open ("coppie.txt");
  file << "graph {\n";
  for (Arco* edge : grafo->archi){
    file << '\t' << (edge->u)->id << " -- " << (edge->v)->id;
    //se l'arco è nel matching lo coloro di rosso
    if(((edge->u)->mate) == edge->v){
        file << '\t' << "[color=red]" << ";\n";
        coppie << (edge->u)->id << " -- " << (edge->v)->id << "\n";
    }
    else
      file << ";\n";
    }
  file << "}";
  file.close();
  coppie.close();

  /*
  Ho aggiunto la parte che fa tutto da sola e apre il .png
  Se non funziona, commentarla e compilare a mano
  */
  system("dot -Tpng:cairo:gd grafo.dot -o grafo.png");
  system("eog grafo.png &");
}


void generaGrafoCasuale(int size, int densita, Grafo* grafo){


  if(densita < 1 || densita > 100)
    throw "Densita non valida!";

  srand (time(NULL));

  ofstream file;
  file.open ("archi.txt");

  //creazione archi
  for(int u = 1 ; u <= size ; u ++)
    for(int v = u+1 ; v <= size ; v ++)
        if(densita > rand() % 100)
          file << u << " " << v << "\n";

    file.close();
    generaGrafoDatiArchi(grafo,"archi.txt");
}

void generaGrafoDatiArchi(Grafo* grafo, string nomeFile){

    ifstream inFile(nomeFile);

    if (inFile.fail()) {
        cerr << "File non valido" << endl;
        exit(1);
    }

    map<string, Nodo*> mp;
    map<string, Nodo*>::iterator it;

    string id1;
    string id2;

    while(inFile >> id1){
        inFile >> id2;
        Nodo* a = new Nodo;
        Nodo* b = new Nodo;

        //controllo se i due nodi esistono già
        it = mp.find(id1);
        if(it == mp.end()){
            a->id = atoi(id1.c_str());
            mp[id1] = a;
        } else {
            a = it->second;
        }

        it = mp.find(id2);
        if(it == mp.end()){
            b->id = atoi(id2.c_str());
            mp[id2] = b;
        } else {
            b = it->second;
        }

        //aggiungo arco
        Arco* e = new Arco;
        e->u = a;
        e->v = b;
        (grafo->archi).push_back(e);

        //aggorno vicinato
        (a->vicinato).push_back(e);
        (b->vicinato).push_back(e);
    }

    it = mp.begin();
    while (it != mp.end()){
        (grafo->nodi).push_back(it->second);
        it++;
    }
    inFile.close();

}
