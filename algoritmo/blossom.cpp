#include "funzioni.h"

Nodo* nullo;

void setBaseElementi(Nodo** u, Nodo ** v, Nodo** base, vector<Nodo*> &elem){
    vector<Nodo*> p1;
    vector<Nodo*> p2;
    percorso(u,p1);
    percorso(v,p2);

    //la base è il primo comune nel cammino nodo-radice
    bool stop = false;
    int pos1 = 0;
    int pos2 = 0;
    int size = p2.size();
    for(int x = 0; ; x++){
        for(int y = 0; y< size; y++)
            if(p1[x] == p2[y]){
                pos1 = x;
                pos2 = y;
                stop = true;
                break;
            }
        if(stop) break;
    }
    *base = p1[pos1];
    //gli elementi sono tutti quelli prima della base
    pos1--;
    for(int i = pos1; i > -1 ; i--)
        elem.push_back(p1[i]);
    for(int i = 0; i < pos2; i++)
        elem.push_back(p2[i]);
}

void blossom(Grafo* g, Nodo** v, Nodo** w, int* tempo){
    cout << "Trovato blossom\n";
    //creazione e inizializzazione blossom
    Blossom* b = new Blossom;
    //negli elementi non metto la base
    b->timestamp = *tempo;
    //cout << "blossom con ts : " << b->timestamp << "\n";
    *tempo = *tempo + 1;
    setBaseElementi(v,w, &(b->base), b->elementi);
    b->setInnestati();
    //alterazione Grafo
        //aggiungo il blossom ad ogni nodo e rendo il nodo inattivo
    //per la base
    ((b->base)->blossom).push_back(b);
    (b->base)->attivo = false;
    set<Nodo*> vicinato_provvisorio;
    Nodo* y = new Nodo;
    //per tutti gli altri
    for(Nodo* x: b->elementi){
        (x->blossom).push_back(b);
        x->attivo = false;
    }

    cout << "\tbase: " << (b->base)->id << "\n";
    cout << "\telementi: ";
    for(Nodo* n : b->elementi)
            cout << n->id << " ";
    cout << "\n";

}

bool adiacente_esteso(Nodo**  u, Nodo** v, Blossom* b){

    Nodo* n = new Nodo;
    int prima  = 0;
    while((*u)->blossom[prima] != b)
        prima++;
    prima--;

    if(prima != -1 && (*u)->blossom[prima]->base == (*u)){
        vector<Arco*> vicinato;
        (*u)->blossom[prima]->getVicinato(vicinato, b->timestamp - 1);
        cout << "\t";
        for(Arco* e: vicinato){
            e->estraiNodo(u, &n);
            cout << n->id << " ";
            if( n == (*v))
                return true;
        }

    }
    return false;
}

bool adiacente_reale(Nodo**  u, Nodo** v, Blossom* b){

    Nodo* n = new Nodo;

    for(Arco* e: (*u)->vicinato){
        e->estraiNodo(u, &n);
        if( n == (*v))
            return true;
    }
    return false;
}

//seleziona il percorso adeguato da seguire, se da dx o sx rispetto all'origine del cammino
void percorso_adeguato(vector<Nodo*> &visitati, Nodo** inizio, Nodo** fine,Blossom* b){
    vector<Nodo*> daControllare;
    daControllare.push_back(b->base);
    for(Nodo* n : b->elementi)
        daControllare.push_back(n);
    int x = 0;
    int y = 0;
    int size = daControllare.size();
    for (int i = 0; i < size; i ++){
        if(daControllare[i] == *inizio)
            x = i;
        if(daControllare[i] == *fine)
            y = i;
    }

    if(x <= y){
        if((x-y)%2 == 0){
            for(int i = x; i <= y; i++)
                visitati.push_back(daControllare[i]);
            return;
        } else {
            for(int i = x; i >= 0; i--)
                visitati.push_back(daControllare[i]);
            for(int i = size-1; i >= y; i--)
                visitati.push_back(daControllare[i]);
            return;
        }
    }else{
        if((y-x)%2 == 0){
            for(int i = x; i >= y; i--)
                visitati.push_back(daControllare[i]);
            return;
        } else {
            for(int i = x; i < size; i++)
                visitati.push_back(daControllare[i]);
            for(int i = 0; i <= y; i++)
                visitati.push_back(daControllare[i]);
            return;
        }
    }

}


void cerca(Nodo** start, Nodo** stop, Blossom* b ,vector<Nodo*> &path){

    vector<Nodo*> visitati;
    bool trovato = false;
    //cerco il nodo da cui parto ad attraversare il blossom
    Nodo* inizio = new Nodo;
    if (start == &nullo){
        inizio = b->base;
        trovato = true;
    }
    else if (adiacente_reale(&(b->base), start,b)){
        inizio = b->base;
        trovato = true;
    }
    else
        for(Nodo* n : b->elementi)
            if(adiacente_reale(&n, start,b)){
                inizio = n;
                trovato = true;
                break;
            }
    if(!trovato){
        if (adiacente_esteso(&(b->base), start,b))
            inizio = b->base;
        else
            for(Nodo* n : b->elementi)
                if(adiacente_esteso(&n, start,b)){
                    inizio = n;
                    break;
                }
    }
    //cerco di uscire dal blossom
    Nodo* fine = new Nodo;
    trovato = false;
    if (stop == &nullo){
        fine = b->base;
        trovato = true;
    } else if (adiacente_reale(&(b->base), stop,b)){
        trovato = true;
        fine = b->base;
    }
    else
        for(Nodo* n : b->elementi)
            if(adiacente_reale(&n, stop,b)){
                fine = n;
                trovato = true;
                break;
            }

    if(!trovato){
        if (adiacente_esteso(&(b->base), stop,b))
            fine = b->base;
        else
            for(Nodo* n : b->elementi)
                if(adiacente_esteso(&n, stop,b)){
                    fine = n;
                    break;
                }
    }


    //determina gli elementi
    percorso_adeguato(visitati, &inizio, &fine, b);
    int passi = visitati.size();
    //passo ricorsivo: se ci sono altri blossom, li espando
    int curr_blossom;
    for(int pos = 0; pos < passi; pos++){
        curr_blossom = 0;
        while((visitati[pos]->blossom)[curr_blossom] != b)
            curr_blossom++;
        if(curr_blossom > 0){
            if (passi == 1)
                attraversa_blossom(start, stop, (visitati[pos]->blossom)[curr_blossom - 1], path);
            else if(pos == 0)
                attraversa_blossom(start, &visitati[pos+1], (visitati[pos]->blossom)[curr_blossom - 1], path);
            else if (pos == passi - 1)
                attraversa_blossom(&visitati[pos-1], stop, (visitati[pos]->blossom)[curr_blossom - 1], path);
            else
                attraversa_blossom(&visitati[pos-1], &visitati[pos+1], (visitati[pos]->blossom)[curr_blossom - 1], path);
        } else
            path.push_back(visitati[pos]);
    }

}

/*
-start e stop sono fuori dal Blossom
-se uno dei due vale null allora cerco/parto dalla radice
*/
void attraversa_blossom(Nodo** start, Nodo** stop, Blossom* b, vector<Nodo*> &path){
    cout << "Attraversamento blossom\n";
    cout << "\tbase: " << (b->base)->id << "\n";
    cout << "\telementi: ";
    for(Nodo* n : b->elementi)
            cout << n->id << " ";
    cout << "\n";
    cerca(start, stop, b , path);
}

void gestisci_espansione(vector<Nodo*> &tmp, vector<Nodo*> &path){

    int size = tmp.size();
    for(int pos = 0; pos < size; pos++)
        if(tmp[pos]->attivo)
            path.push_back(tmp[pos]);
        else
            if(pos == 0)
                attraversa_blossom(&nullo, &tmp[pos+1], (tmp[pos]->blossom).back(), path);
            else if (pos == size - 1)
                attraversa_blossom(&tmp[pos-1], &nullo, (tmp[pos]->blossom).back(), path);
            else
                attraversa_blossom(&tmp[pos-1], &tmp[pos+1], (tmp[pos]->blossom).back(), path);
}
