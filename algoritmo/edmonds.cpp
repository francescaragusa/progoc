#include "funzioni.h"

void edmonds(Grafo* grafo){

    cout << "***INIZIO***\n";

    vector<Nodo*> cammino;
    int* tempo = new int(1);
    while(esploraGrafo(grafo, cammino, tempo))
        if(cammino.size()>0){
            //se è zero allora ho trovato un blossom
            aumentaMatching(cammino);
            grafo->ncoppie += 1;
            grafo->eliminaBlossom();
        }

    cout << "Non sono stati trovati altri cammini aumentanti\n";
    cout << "------------\n";
    cout << "Cardinalità del matching: " << grafo->ncoppie <<"\n";
    cout << "------------\n";
    cout << "***FINE***\n";
}

void aumentaMatching(vector<Nodo*> &cammino){
    int size = cammino.size();
    cout << "Nodi del c.a: ";
    for(int pos =0 ; pos < size; pos = pos+2){
        cammino[pos]->mate = cammino[pos+1];
        cammino[pos+1]->mate = cammino[pos];
        cout << cammino[pos]->id <<" " << cammino[pos+1]->id << " ";
    }
    cout << "\n";
}

void percorso(Nodo** n, vector<Nodo*> &path){
    Nodo* s = new Nodo;
    s = *n;
    path.push_back(s);
    while(s != s->radice){
        s = s->padre;
        path.push_back(s);
    }
}

void aggiornaForesta(Foresta* f, vector<Nodo*> &daControllare, Nodo** v, Nodo** w){
    Nodo* x = new Nodo;
    x = (*w)->mate;

    if(!x->attivo)
        while(x != ((x->blossom).back())->base)
            x = ((x->blossom).back())->base;

    if(*w == *v)
        return;

    f->makeset(w);
    f->treeUnion(v,w);

    if(*w == x || *v == x)
        return;


    f->makeset(&x);
    daControllare.push_back(x);
    f->treeUnion(w, &x);

}

void getCammino(Nodo** v, Nodo** w, vector<Nodo*> &p){
    cout << "Trovato cammino aumentante da " << (*v)->radice->id << " a " << (*w)->radice->id << "\n";
    vector<Nodo*> tmp;
    percorso(v,tmp);
    reverse(tmp.begin(),tmp.end());
    percorso(w,tmp);
    gestisci_espansione(tmp,p);
}

bool esploraGrafo(Grafo* grafo, vector<Nodo*> &cammino, int* tempo){

    cout << "------------\n";
    cout << "Iniziata esplorazione grafo\n";

    cammino.clear();

    //creo foresta con nodi esposti come alberi singoli
    Foresta foresta;
    for(Nodo* n : grafo->nodi){
        n->clear();
        if(n->mate == nullptr)
            foresta.makeset(&n);
    }


    //marco gli archi nel matching
    for(Arco* a : grafo->archi){
        if((a->u)->mate == (a->v))
            a->marcato = true;
        else
            a->marcato = false;
    }

    //più facile iterare su una lista e aggiungere/togliere gli elementi man mano
    vector<Nodo*> daControllare = foresta.elementi;

    while(! daControllare.empty()){
        //estraggo un nodo
        Nodo* v = daControllare.front();
        Nodo* w = new Nodo;
        daControllare.erase(daControllare.begin());
        vector<Arco*> vicinato;

        if(v->attivo)
            vicinato = v->vicinato;
        else
            ((v->blossom).back())->getVicinato(vicinato, -1);

        for(Arco* e : vicinato){
            if(!e->marcato){
                e->estraiNodo(&v,&w);

                //se è un nodo contratto vado alla base
                if(!w->attivo)
                    while(w != (w->blossom).back()->base)
                        w = (w->blossom).back()->base;
                if(w->distanza == -1)
                    aggiornaForesta(&foresta, daControllare, &v, &w);
                else if((w->distanza) % 2 == 0){
                    if(foresta.find(&v) != foresta.find(&w))
                        getCammino(&v,  &w, cammino);
                    else
                        blossom(grafo, &v,&w, tempo);
                    return true;
                }
                e->marcato = true;
            }
        }
    }

    return false;

}
