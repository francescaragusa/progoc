#ifndef FUNZIONI_H
#define FUNZIONI_H

#include "../generazioneGrafo/Grafo.h"
#include <vector>
#include <set>
#include <iostream>

using namespace std;
void edmonds(Grafo* grafo);
void aumentaMatching(vector<Nodo*> &cammino);
void aggiornaForesta(Foresta* f, vector<Nodo*> &daControllare, Nodo** v, Nodo** w);
void getCammino(Nodo** v, Nodo** w, vector<Nodo*> &p);
bool esploraGrafo(Grafo* grafo, vector<Nodo*> &cammino, int* tempo);
void percorso(Nodo** n, vector<Nodo*> &path);

void blossom(Grafo* g, Nodo** v, Nodo** w, int* tempo);
void setBaseElementi(Nodo** u, Nodo ** v, Nodo** base, vector<Nodo*> &elem);
bool adiacente_reale(Nodo**  u, Nodo** v, Blossom* b);
bool adiacente_esteso(Nodo**  u, Nodo** v, Blossom* b);
void percorso_adeguato(vector<Nodo*> &elementi, Nodo** inizio, Nodo** fine,Blossom* b);
void cerca(Nodo** start, Nodo** stop, Blossom* b ,vector<Nodo*> &path, bool reverse);
void attraversa_blossom(Nodo** start, Nodo** stop, Blossom* b, vector<Nodo*> &path);
void gestisci_espansione(vector<Nodo*> &tmp, vector<Nodo*> &path);

#endif
