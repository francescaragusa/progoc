#include "../generazioneGrafo/Grafo.h"
#include "../algoritmo/funzioni.h"
#include <string>

int main(int argc, char *argv[]){

    Grafo *g = new Grafo;

    if(*argv[1] == 'c' and argc == 4){
        //grafo casuale
        int size = stoi(argv[2]);
        int d = stoi(argv[3]);
        generaGrafoCasuale(size,d,g);
    } else if (*argv[1] == 'f' and argc == 3){
        //grafo da file
        string nomeFile = argv[2];
        generaGrafoDatiArchi(g, nomeFile);
    } else {
        cerr << "Paramentri non validi\n";
        exit(1);
    }

    edmonds(g);
    printGrafo(g);
    return 0;

}
